package pizzas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

class TestPizzas {

	private BasePizzas base=new BasePizzas();

	TestPizzas() throws NoSuchAlgorithmException {
	}

	@BeforeEach
	public void init() {
		base.addPizzaToMenu(base.createSurpriseWhitePizza());
	}
	
	@Test
	void testAjoutPizza() {
		Pizza p=new Pizza("fromages", 10);
		p.ajoutIngredient(new Ingredient("Mozzarelle", true));
		p.ajoutIngredient(new Ingredient("Talegio", true));
		
		base.addPizzaToMenu(p);
		assertEquals(p,base.getPizzaFromMenu("fromages"));
	}
	
	@Test
	 void testAjoutIng1() {
		Pizza p=base.getPizzaFromMenu("Surprise blanche");
		System.out.println(p.formattedIngredients());
		var oldSize=p.ingredients().length;
		p.ajoutIngredient(new Ingredient("brocolis", true));
		assertEquals(oldSize+1,p.ingredients().length);
	}

	@Test
	void testpourcommitoublieça() {
		assertTrue(true);
	}

}
